﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Atestat_patratel
{
    public partial class Form1 : Form
    {
        
        int miscare = 1;
        int i = 0;
        int k = 0;
        int level = 100;
        int j;
        int scor = 0;
        Random poz = new Random();
        Label[] cerc = new Label[50000];
        int[] viz = new int[50000];
        int[] mod = new int[10];


        public Form1()
        {
            InitializeComponent();
           this.KeyDown += new KeyEventHandler(Form1_KeyDown);
           label1.BackColor = Color.Transparent;
           label1.Parent = pictureBox2;
        }


                             //miscarea lui patratel
        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (miscare == 1)
            {
                if ((e.KeyCode == Keys.Left || e.KeyCode == Keys.A) && patratel.Location.X > 10)
                { patratel.Left -= 3; }
                else if ((e.KeyCode == Keys.Right || e.KeyCode == Keys.D) && patratel.Location.X < 370)
                { patratel.Left += 3; }
            }

        }
                               //miscarea lui patratel


                               //butonul de Start
        private void button1_Click(object sender, EventArgs e)
        {
            pictureBox1.Visible = true;
            patratel.Visible = true;
            scoruri.Visible = true;
            button1.Visible = false;
            cerculet.Visible = true;
            pictureBox2.Visible = false;
            label1.Visible = false;

            this.timer1.Tick += new EventHandler(timer1_Tick);
            timer1.Interval = 10;
            this.timer1.Start();
            scoruri.Visible = true;
            scoruri.Text = "0";
            miscare = 1;
            for (j = 1; j <= k; j++)
                this.Controls.Remove(cerc[j]);
            k = 0;
            j = 0;
            level = 100;
            scor = 0;
        }


                                          //generare cerculete
        private void timer1_Tick(object sender, EventArgs e)
        {
            int pozitie;
            if (i > 300)
            {
                timer1.Interval = 5;
                level = 50;
            }
            if (i % level == 0)
            {   
                pozitie = poz.Next(20, 400);
                cercuri(pozitie, 38, 10);
            }
            i++;
            if ( i%5==0)
                for(j=1;j<=k;j++)
                {
                    cerc[j].Top = cerc[j].Top + 4;
                    if (cerc[j].Bounds.IntersectsWith(patratel.Bounds))
                    {
                        timer1.Stop();
                        miscare = 0;
                        MessageBox.Show("Sfarsitul jocului. Ai obtinut " + Convert.ToInt32(scoruri.Text) + " puncte.");
                        
                    }

                    if(cerc[j].Top > 38 + 405 && viz[j]==0)
                    {
                        scor++;
                        scoruri.Text = scor.ToString();
                        viz[j] = 1;
                    }
                }
        }
                                            //generare cerculete
        private void cercuri (int pozx, int pozy, int viteza)
        {
            Label cerculet = new Label();
            System.Drawing.Drawing2D.GraphicsPath XY = new System.Drawing.Drawing2D.GraphicsPath();
            XY.AddEllipse(0,0,20,20);
            cerculet.Region = new Region (XY);
            this.Controls.Add(cerculet);
            cerculet.BringToFront();
            cerculet.Size = new Size(20, 20);
            cerculet.Left = pozx;
            cerculet.Top = pozy;
            cerculet.BackColor = Color.Blue;
            cerculet.Visible = true;
            cerculet.Enabled = true;
            cerc[++k] = cerculet;
            viz[k] = 0;
        }
                                               //generare cerculete



    }
}
